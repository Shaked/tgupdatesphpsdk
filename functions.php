<?php

if (!function_exists('tgupdates')) {
/**
 * @param $options
 * @param $params
 * @return mixed
 */
    function tgupdates($chatId, $token, $text, $options = [], array $params = []) {
        if (!isset($options['active']) || !$options['active']) {
            return;
        }

        $params['text'] = $text;
        $params = [
            'params' => $params,
        ];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $options['baseUrl'],
            'timeout'  => $options['timeout'],
        ]);

        $uri = str_replace([
            '{{ chatId }}',
            '{{ token }}',
        ], [
            $chatId,
            $token,
        ], '/telegram/appUpdates?chatId={{ chatId }}&token={{ token }}'
        );

        return $client->post(
            $uri,
            [
                'form_params' => $params,
            ]
        );
    }
}